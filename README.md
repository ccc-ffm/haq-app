# The CCC FFM H[a]Q App

![Controlz Alll teh Tings!](doc/photofunia-1476355400.jpg)

# Änderungen machen

Bitte erzeugt einen Branch (oder Fork) auf dem ihr arbeitet. Änderungen am master Branch werden nur via Merge-Request eingepflegt.

# Binaries

Dieses repository nutzt Gitlab Ci um für jede Änderung binaries zu kompilieren.

## Android

Die "release" version aus dem master Branch ist in folgendem F-Droid-kompatibles Repo zu finden:
https://fdroid.chaos.expert

Derzeit kaputt: Für jeden Branch wird unter https://fdroid.chaos.expert/haq/$BRANCHNAME ebenfalls ein F-Droid repo angelegt.

## IOS

 TBD


# License

Licensed under the Apache License, Version 2.0. See the [LICENSE] file.
