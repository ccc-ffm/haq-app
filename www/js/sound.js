function sound(value) {
    $.get('http://192.168.2.12/cgi-bin/lirc.sh/Verstaerker?' + value);
}

$('#sound-control, #sound-source').find('button').click(function () {
    var repeat = 1;
    if ($(this).attr('data-repeat')) {
        repeat = $(this).attr('data-repeat');
    }
    for (var i = 0; i < repeat; i++) {
        sound($(this).attr('data-button'));
    }
});
