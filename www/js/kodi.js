/**
 * Kodi rpc client
 */
(function ($, window) {
    "use strict";

    window.kodi = {
        /**
         * Default options
         */
        default_options: {
            url: 'http://192.168.2.123/jsonrpc',
            id: 1,
            success: function () {
            }
        },

        /**
         * Send JSON-RPC Request to RasPi
         *
         * @param method string
         * @param params mixed
         * @param options object
         */
        request: function (method, params, options) {
            var settings = $.extend({}, this.default_options, options);

            if (typeof method === 'undefined') {
                console.error('method not specified');
                return this;
            }

            var requestData = {
                jsonrpc: '2.0',
                id: settings.id,
                method: method
            };

            if (typeof params !== 'undefined') {
                requestData.params = params;
            }

            var requestUrl = settings.url + '?request=' + this.encodeUrl(JSON.stringify(requestData));

            return $.ajax({
                url: requestUrl,
                success: settings.success
            });
        },

        /**
         * Replace ? and & from urls
         *
         * @param text string
         * @returns {string}
         */
        encodeUrl: function (text) {
            return text
                .split('&').join('%26')
                .split('?').join('%3F');
        },

        /**
         * Send a key press
         *
         * @see http://kodi.wiki/view/JSON-RPC_API/v6#Input.Action
         *
         * @param key string
         * @param options object
         */
        sendKey: function (key, options) {
            var data = {
                action: key
            };

            this.request('Input.ExecuteAction', data, options);
        },

        /**
         * Open a media file
         *
         * @param url string
         * @param options object
         */
        playMedia: function (url, options) {
            var data = {
                item: {
                    file: url
                }
            };

            this.request('Player.Open', data, options);
        }
    };
}(jQuery, window));
