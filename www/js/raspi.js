/**
 * Bind to button press
 */
$('#mediaPi-sound, #mediaPi-control').find('button').click(function () {
    var key = $(this).data('key');
    kodi.sendKey(key);
});

$('#mediaPi-media-play').click(function () {
    var url = $('#mediaPi-media-url').val();

    if (url == '') {
        return;
    }

    var regex = {
        youtube: /^(?:https?:\/\/)?(?:(?:www|m)\.)?(?:youtube\.com|youtu\.be)\/(?:[\w\-]+\?v=|embed\/|v\/)?([\w\-]+)(\S+)?$/i,
        soundCloud: /^(?:https?:\/\/)?(?:(?:www|m)\.)?(soundcloud\.com|snd\.sc)\/(.*)$/i,
        vimeo: /^(?:https?:\/\/)?(?:(?:www|player)\.)?vimeo\.com\/(?:channels\/(?:\w+\/)?|groups\/(?:[^\/]*)\/videos\/|album\/(?:\d+)\/video\/|video\/|)(\d+)(?:$|\/.*|\?.*)$/i
    };

    var youtube = regex.youtube.exec(url),
        soundCloud = regex.soundCloud.exec(url),
        vimeo = regex.vimeo.exec(url);

    if (youtube != null) {
        kodi.playMedia('plugin://plugin.video.youtube/?action=play_video&videoid=' + youtube[1])
    } else if (soundCloud != null) {
        kodi.playMedia('plugin://plugin.audio.soundcloud/play/?url=https://' + soundCloud[1] + '/' + soundCloud[2])
    } else if (vimeo != null) {
        kodi.playMedia('plugin://plugin.video.vimeo/play/?video_id=' + vimeo[1])
    } else {
        kodi.playMedia(url)
    }
});

$('#mediaPi-media-play-sofa-fm').click(function () {
    kodi.playMedia('https://sofa.fm');
});
